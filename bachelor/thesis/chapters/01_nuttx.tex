\label[nuttx]
\chap NuttX

This chapters describes NuttX real time operating system used in this thesis. The reader is introduced to basic information regarding NuttX source
code organization, device driver's system and compilation steps.

\sec Introduction

NuttX is an open source real time operating system (RTOS) first introduced by Gregory Nutt in 2007. It has come a long way since
that and has been undergoing incubation at The Apache Software Foundation since 2019. NuttX is written in C language and offers a \glref{POSIX} compatible
environment.\cite[nuttx-article] This means the applications written in compilance with \glref{POSIX} standards (on GNU/Linux for example) can be run on NuttX as well
with minimal changes. Furthemore it implements \glref{ANSI} standards and some further \glref{API}s from Unix systems or some other real time operating systems like
VxWorks.

As described in the docoumentation, NuttX is scalable from small 8 bits to modern 64 bits microcontrollers.\cite[NuttX-about] This is allowed by linking from
static libraries and using many source files that contain only small number of functions. The disadvantage of this aproach is less clear source code but it
allows the build system to link only those functions desired by the user. According to a NuttX documentation, the final executable can than be run for
example on only 32 kB total memory (code and data) although typical NuttX build usually requires about at least 64 kB memory.\cite[NuttX-about]

The desired functions like additional drivers (\glref{ADC}, \glref{CAN}, Ethernet), applications or systems features (tickless mode) can be selected using
Kconfig system which is taken from Linux Kernel. The configuration system offers a great freedom in choosing which features should be
included in NuttX, on the other hand it sometimes lacks proper documentation and thus sometimes it is not user friendly. Some of these
configurations also does not work well together and can result in build error.

\sec Source Code Organization

The NuttX directory structure takes a lot of inspiration from Linux kernel.\cite[NuttX-dir-structure] The most important folders from the
perspective of this thesis are subdirectories "arch/", "boards/" and "drivers/". These subdirectories contain source code and header files
for supported architectures, microcontrollers, boards and drivers. The following sections shortly describe each of those subdirectories. The
goal is to provide basic understanding regarding NuttX source code and driver's organization. This knowledge is important for the implementation of
driver's to NuttX mainline which is described in chapter \ref[implementation].

The following sections discuss source code organization only from the device drivers' point of view. NuttX core itself is not taken into account
here as this goes beyond the scope of the thesis.

\label[nuttx-arch]
\secc Arch

The subdirectory "arch/" contains folders "include/" and "src/" for each supported microcontroller.\cite[NuttX-dir-structure] Folder
"include/" includes basic microcontroller definitions like which peripherals are supported, external interrupts or peripheral identificators.
The more important folder from the point of this thesis is "src/" which includes all source files for hardware specific implementation for
supported drivers (also called "lower half" in NuttX documentation).\cite[NuttX-devices]

This is where NuttX varies from Linux. While Linux kernel usually implements a single driver for all architectures and machines using the same IP core for
given peripheral, NuttX strictly implements those controllers for each microcontroller. This difference can be shown with the following example.

\nl
{\bf Example:}
Let's take a FlexCAN controller for the purpose of this example. Linux has driver for this controller located
in common file "drivers/net/can/flexcan.c" while NuttX implements "flexcan.c" files in both "arch/arm/src/imxrt/" and "arch/arm/src/s32k1xx/" \glref{MCU}s
subdirectories. Structures and functions from those files are connected to the "upper half" part of the driver which provides a high-level \glref{POSIX} interface
(write, read, etc.).
\nl

Every lower half part of the driver is usually divided into three separate files. The consensus in NuttX community is to keep the names
in format "mcu_driver.c", "mcu_driver.h" and "hardware/mcu_driver.h".

\nl
{\bf Example:}
The names for \glref{ADC} driver for imxRT \glref{MCU} would be "imxrt_adc.c", "imxrt_adc.h" and "hardware/imxrt_adc.h".
\nl

The first ".c" file contains source code for the driver and takes care of setting up the peripheral and interface with driver's upper half described later in
this chapter. The first header file usually defines just one function which can be accessed from board level. This function takes care of initial setting of the
driver and usually also returns the driver instance so it can be registered by the upper half. The latter header file located in "hardware" subfolder contains
definitions of peripheral's registers and bit fields.

\secc Boards

The second subdirectory mentioned in this thesis is "boards/". This includes all necessary source files and header files for board level support
such as booting process or parts of the code that call functions from "arch/" section that initialize drivers. Board specific implementation is not discussed
in the thesis. However implemented drivers were included in already exising board support packages, the initialization and compilation of a specific board is
desribed later in the thesis.

\secc Drivers

The last section of the three mentioned is "drivers/" which contains files for the "upper half" parts of the drivers as named per
NuttX documentation. The "upper half" registers itself a device name ("dev/adc0", "dev/mcan0") via Virtual File System and implements
the high level interface such as "read", "write" or "open". The interface between the "upper half" and "lower half" is mediated via callbacks to the
"lower half" part of the driver.\cite[NuttX-devices]

\label[nuttx-plat]
\sec Supported Platforms and MCUs

This section contains a non exhaustive list of platforms and \glref{MCU}s supported by NuttX. Only platforms and \glref{MCU}s interesting from the point of
view of this thesis are mentioned here, the complete list can be found in NuttX documentation or in NuttX source code.

The most extended support in NuttX is for ARM instruction set architecture (ISA) nowadays used in most of the microcontrollers for embedded
systems.\cite[mcu-arm] This includes versions of STM32 chips, microcontrollers from imxRT series, LPC series or SAM series. Other architectures like
Xtensa with support for microcontrollers designed by Espressif company, widely known ISA x86 or open standard architecture RISC-V are also supported but
so far not as widely as ARM.

\label[nuttx-comp]
\sec NuttX Compilation

The following steps are required to compile NuttX. The compilation steps are documented for Linux distribution Ubuntu (version 20.04 and
newer) but this should be the same for other Debian based Linux distributions. The compilation requires the kconfig-frontend package to be installed
on the system.\urlnote{https://packages.ubuntu.com/focal/kconfig-frontends} Architecture specific toolchain (e.g. arm-none-eabi-gcc for ARM targets) is also
required. Compilation of NuttX on Windows or Mac OS is also possible and is described in NuttX documentation.

\begtt
git clone https://github.com/apache/incubator-nuttx.git nuttx
git clone https://github.com/apache/incubator-nuttx-apps.git apps
cd nuttx
./tools/configure.sh board:config
make
\endtt

Where "board" represents the name of the board (also the name of the directory where board's files are located) and "config" represents the name for
required configuration. The configurations available for certain board can be found in NuttX documentation for that board or by looking directly to
the source code.

\nl
{\bf Example:}
The basic configuration with serial port console for Teensy 4.1 board would be "./tools/configure.sh teensy-4.x:nsh-4.1". The configuration file can be
found in "boards/arm/imxrt/teensy-4.x/configs/" directory.
\nl

The described steps compile NuttX executable "nuttx.bin" or hexadecimal source file "nuttx.hex" based on the target hardware.
