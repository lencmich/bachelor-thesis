\label[pysimcoder]
\chap PysimCoder

This chapter introduces a software tool pysimCoder and its block editor. The Linux and NuttX compilation steps of a block diagram designed in pysimCoder
are described in following sections as well as pysimCoder code generation process. The understanding of the code generation process is very useful as some
changes were made in this area during the Silicon Heaven infrastructure implementation. The source code organization is also mentioned here. While it is not
important for this thesis, its knowledge can provide to be useful and helpful. It also becomes necessary for the implementation of future blocks into
pysimCoder.

\sec Introduction

PysimCoder is an open source control application development tool designed by Professor Roberto Bucher from University of Applied Sciences and Arts of Southern
Switzerland. The application consists of an extended python-control library and a graphical block editor with a code
generator.[\rcite[Bucher-pysimCoder], chapter 7] As mentioned by Professor Bucher at NuttX Online Workshop 2021, the extension of
python-control library allows integration of control design and simulaton methods as full and reduced state space observer, anti-windup mechanism and discrete
linear quadratic regulator. These methods are highly useful for the implementation of real time controllers.\cite[pysim-nuttx-conf] PysimCoder can also perform
the simulation of designed block diagram.

The pysimCoder functionality for real time control was tested and demonstrated by Professor Roberto Bucher on systems like second order RCRC plant or inverted
pendulum.\cite[pysim-pendulum]\cite[pysim-rcrc] The system is also used at control theory courses at University of Applied Sciences and Arts of Southern
Switzerland.\cite[pysim-nuttx-conf]

\sec Block Editor

PysimCoder's block diagram editor GUI is in many ways similar to popular Simulink editor. The editor consists of two separate windows, diagram window and library.
Library window includes all available blocks separated to several libraries (Math, NuttX and input for example). These blocks can be moved to diagram window using
drag and drop method.[\rcite[Bucher-pysimCoder], chapter 7] Both windows are shown in Figure 3.1.

Most of the blocks have specific parameter options and can support various number of inputs/outputs. The parameter settings is opened by double left click
on the block placed in a diagram window. This allows the user to set parameters like controller's gain constants, driver's device name and so on. The selection
of number of inputs/outputs can be accessed via single right click on the block. The option for multiple inputs/outputs might not be supported for all blocks
(NuttX encoder block offers only one output for example).\cite[NuttX-pysim-doc]

\medskip \clabel[pysim-window]{PysimCoder's library and diagram window.}
\picw=12cm \cinspic figs/pysim-window.png
\caption/f PysimCoder's library (left) and diagram window (right) (Source: [\rcite[NuttX-pysim-doc]]).
\medskip

The extensive manual with the steps required to create an own block diagram in pysimCoder can be found in chapter 7 of Professor Bucher's book Python for
Control Purposes.\cite[Bucher-pysimCoder]

\label[pysim-org]
\sec Source Code Organization

This section provides basic introduction of pysimCoder's source code structure and organization. The goal is to give a basic understanding that would allow
the reader to design and create own custom block without major obstacles. Basic organization of folders taking care of code generation is also shortly mentioned
but is fully introduced in section \ref[pysim-gen]. This focus mainly on code organization, code generation and block's lifecycle is described in
section \ref[pysim-gen]. It is recommended to remind this section once again after reading the section \ref[pysim-gen] to get the complex picture.

PysimCoder has two blocks related folders in its structure, "resources" and "CodeGen". The first mentioned folder provides blocks' declaration in JSON format and
Python part of the code while the latter contains Makefile templates and C parts of blocks' code for supported targets. Both subdirectories are described in the
following sections.

\label[pysim-res]
\secc resources

The first declaration of the block is provided in "resources/blocks/blocks/library" folder where library stands for actual library the block belongs to (i.e
Math, NuttX, etc.). This folder contains ".xblk" files in JSON format that provides declaration of block's inputs, outputs, parameters and so on. The list of
required keys follows.

\begitems
\style 0
* lib – the name of the library the block belongs to
* name – the name of the block
* ip – number of inputs
* op – number of outputs
* stin – 1 if number of inputs can be set by user, 0 otherwise
* stout – 1 if number of outputs can be set by user, 0 otherwise
* icon – name of the icon located in "resources/icons" folder
* params – name of block related Python function followed by the list of parameters
* help – user help
\enditems

{\bf Example:}
Lets take a look into an example of block definiton in JSON format. File
"nuttx_PWM.xblk"\urlnote{https://github.com/robertobucher/pysimCoder/blob/master/resources/blocks/blocks/NuttX/nuttx_PWM.xblk} can be found
in "resources/blocks/blocks/NuttX" folder with the following definition.
\begtt
{
 "lib": "NuttX",
 "name": "PWM",
 "ip": 1,
 "op": 0,
 "stin": 1,
 "stout": 0,
 "icon": "PWM",
 "params": "nuttx_PWMBlk|Port:'/dev/pwm2'|channels: [1]
                        |PWM freq [Hz]:1000|Umin [V]:0.0
                        |Umax [V]:100.0",
 "help": "Help text"
}
\endtt

The JSON file described above provides the link to Python function ("nuttx_PWMBlk" in our example). These functions are located in
"resources/blocks/rcpBlk/library" where the library naming remains the same as in "blocks/" directory. The following example continues with PWM block
for NuttX.

\nl
{\bf Example:}
The name of the file located in "resources/blocks/rcpBlk/NuttX" is "nuttx_PWMBlk.py". The simplified code can be seen below.
\begtt
import numpy as np
from supsisim.RCPblk import RCPblk
from scipy import size

def nuttx_PWMBlk(pin, port, ch, freq, umin, umax):

  blk = RCPblk('nuttx_PWM',pin,[],[0,0],1,[freq,umin,umax],ch,port)
  return blk
\endtt
The order of parameters passed to nuttx_PWMBlk is exactly the same as the order of parameters in ".xblk" files. The order of parameters passed to RCPblk
function can be seen in "toolbox/supsisim/src/RCPblk.py".\urlnote{https://github.com/robertobucher/pysimCoder/blob/master/toolbox/supsisim/src/RCPblk.py}
It is necessary to pass the parameters in the correct order as the placement in RCPblk functon defines the type of the parameter (integer, double or
string). It is also worth mentioning the first parameter of the function is the name of the corresponding C function and the fifth parameter is
the input to output relation.

\label[pysim-codegen2]
\secc CodeGen

While "resources" directory contains files that defines the block, "CodeGen" located files takes care of the actual execution of block's functions. Once again,
the example is used to present the structure.

\nl
{\bf Example:}
We take a PWM block for NuttX \glref{RTOS} once again. The corresponding file
"nuttx_PWM.c"\urlnote{https://github.com/robertobucher/pysimCoder/blob/master/CodeGen/nuttx/devices/nuttx_PWM.c} is located in "CodeGen/nuttx/devices/"
folder, the largery simplified C code follows.
\begtt
static void init(python_block *block){}
static void inout(python_block *block){}
static void end(python_block *block){}

void nuttx_PWM(int flag, python_block *block)
{
  if (flag == CG_OUT){          /* get input */
    inout(block);
  }
  else if (flag == CG_END){     /* termination */
    end(block);
  }
  else if (flag == CG_INIT){   /* initialisation */
    init(block);
  }
}
\endtt
The enter function "nuttx_PWM" receives an integer flag and a pointer to the block structure. The corresponding function is called based on the receved flag.
Function "init" is called only once and as the function name would suggest it initalizes the driver. Flag CG_OUT calls function "inout" that performs the
required operation. In our example it updates the PWM duty cycle based on the block input, but it could also read data from \glref{ADC} or generate some kind of
an output (square, triangle, etc.). This function is called repeatedly. Function "end" then takes care of stopping the output and closing the driver. The
definition of those functions is not listed here as it is block specific.

These sections desribed the organization of the pysimCoder blocks. The core of the code generation can be found in "toolbox" directory. The principle of
the process is described in the following section.

\label[pysim-gen]
\sec Code Generation

This section provides the description of code generation process from designed block diagram. It is divided into two subsections, general description
regarding the process and more practical point of view on source code organization.

\secc General Description

Every block in pysimCoder can be described by a set of equations representing its internal states and outputs.

$$x_{k+1} = f(x_k, u_k, k) \eqmark$$
$$y_k = g(x_k, u_k, k) \eqmark$$

Two equations listed above are well known as state space equations, however they are execuded in an opposite order. System firstly updates block's output, described
by equation $(2)$ and then internal state per equation $(1)$. Blocks would need to remember theirs previous $x_{k}$ state if equation $(1)$ would be executed first,
this is not necessary if the order is opposite. The first function is also not mandatory as some blocks do not have an internal state. Apart from those
two functions, each block also has an initialization and a termination function. These functions are called throught corresponding flag: CG_OUT, CG_STUPD,
CG_INIT or CG_END.\cite[pysim-nuttx-conf]

The code generation process takes a designed block diagram and an optional Python script (used for implementation of the controller and variables definitions)
and generates a Python script "tmp.py". This script translates the block diagram into C code from which specific blocks functions are called. The diagram of this
process can be seen in Figure 3.2.

\medskip \clabel[pysim-codegen]{PysimCoder: Code Generation Process}
\picw=12cm \cinspic figs/codegen-diag.pdf
\caption/f PysimCoder Code Generation Process (Inspired by: [\rcite[pysim-nuttx-conf]]).
\medskip

The generated C code is subsequently compiled and linked with the block library and main C file with the real time thread and creates an
executable.\cite[pysim-nuttx-conf] The code generation process also needs to find the right execution sequence so the blocks are executed in the
correct order (as blocks' inputs depending on other blocks' outputs can not be executed first).

\secc Source Code Organization

As mentioned at the end of section \ref[pysim-org], main source files for Code Generation are located in "toolbox" directory. This directory contains two libraries,
"supsictrl" and "supsisim". The first mentioned implements control methods as state space observer and others mentioned in the introduction, the
latter one brings the files related to code generation.

The function "generateCCode" in "scene.py" creates the "tmp.py" file mentioned in the previous sections. The sample of the code from "tmp.py" can be seen below.

\begtt
Const = constBlk([2],  3.2)
Gain = matmultBlk([2],[3],  1)
Print = printBlk([4,5,3,1])

blks = [Const,Gain,Print,]

fname = 'test'
os.chdir("./test_gen")
genCode(fname, 0.01, blks)
genMake(fname, 'rt.tmf', addObj = '')

import os
os.system("make")
os.chdir("..")
\endtt

The first three lines of the code calls the block specific function that was introduced to the readed in the previous section. This returns the block
structure which is subsequently passed to "genCode" function located in "RCPgen.py" in "supsisim" library. This function takes care of C code generation,
the third step shown in Figure 3.2. Then corresponding Makefile is generated and required executable is created. Files "scene.py" and "RCPgen.py"
are the ones that were changed during Silicon Heaven implementation. These changes are fully described in chapter 6.

\sec NuttX Integration

This section provides the necessary steps to succesfully run pysimCoder on NuttX. The compilation of NuttX remains the same as described in
section \ref[nuttx-comp] but requires some additional configuration options. These options are described in NuttX documentation.\cite[NuttX-pysim-doc] Once NuttX is
compiled, following commands need to be run.

\begtt
make export
cp nuttx-export-xx.x.x.zip /../pysimCoder/CodeGen/nuttx
cd /../pysimCoder/CodeGen/nuttx
unzip nuttx-export-xx.x.x.zip
mv nuttx-export-xx.x.x.zip nuttx-export
cd devices
make
\endtt

Where xx.x.x in nuttx-export-xx.x.x.zip stands for the current version of NuttX. Execution of "make" command in "devices" folder compiles the C files
described in previous section. Then pysimCoder can be run either via included script "pysim-run.sh" or via application installed on Linux. The instalation
process is described in pysimCoder documentation, the steps required to succesfully create pysimCoder application for NuttX are described in NuttX
documentation.\cite[NuttX-pysim-doc] The important setting is to select a Template Makefile for the target. This can be done in the top menu by clicking on
Block settings icon which is highlighted in the red circle as described in NuttX documentation.\cite[NuttX-pysim-doc] The required Template Makefile for NuttX
is "nuttx.tmf".

\medskip \clabel[pysim-menu]{PysimCoder: Menu Option}
\picw=12cm \cinspic figs/pysim-menu.png
\caption/f PysimCoder Menu Option (Source: [\rcite[NuttX-pysim-doc]]).
\medskip

The loadable executable can be generated by selecting Generate C-code icon highlighted in the green cirle. The executable is a standard NuttX with a terminal,
file system and selected applications plus an application called "main". This is the designed block diagram. The control application can be run from NuttX terminal
simply by the following command.

\begtt
nsh> main
\endtt

The application "main" can also be selected as an init function instead of "nsh_main", providing terminal support, in NuttX configuration. Another option is to
modify initialization scripts inside NuttX system to run generated application automatically.

\secc Supported Blocks

Apart from common blocks, usually from mathematic, input or output libary, pysimCoder implements NuttX specific blocks. These blocks provides an interface
to device driver peripherals such as \glref{ADC}, \glref{PWM} or \glref{DAC}. This chapter provides the list of supported peripherals at the time of writing this
thesis.

\begitems
\style 0
* ADC – Analog to Digital Converter
* CAN – Controlled Area Network, with FD support
* DAC – Digital to Analog Converter
* ENC – Encoder
* GPIO – support for both input and output pins
* PWM – Pulse Width Modulation with multichannel support
* serialOut – support for serial output
\enditems

Some blocks from communication library as TCP and UDP respect POSIX standard and they can be used with NuttX as well.