\label[hw]
\chap Target Hardware Selection

Two microcontrollers, imxRT1060 and SAM E70, were selected as a target hardware for the testing purposes of this thesis. This chapter discusses the
reasons behind this selection and introduces these \glref{MCU}s to the reader. Two microcontrollers, imxRT1060 and SAM E70, both used in the thesis's practical part,
are discussed here.

\sec Introduction

As mentioned in section \ref[nuttx-plat], NuttX provides extended support for microcontrollers with ARM instruction set architecture so the logical step
is to use an ARM based \glref{MCU}. The alternative, interesting from educational point of view, would be the selection of open architecture RISC-V based
\glref{MCU}, for example Espressif ESP32-C3. While the open source status of the instruction set architecture is interesting and promising for the future, the
development of NuttX support for those chips is still ongoing and the \glref{MCU} designed by Espressif also does not provide many pin outputs. This resulted in
selection of ARM based microcontrollers.

Most of the ARM based microcontrollers provide support of peripherals such as \glref{ADC}, \glref{PWM}, Encoder or Ethernet that are needed for the fulfilment of the
thesis's goals. This means the decision of particular microcontrollers being based mostly on the faculty's and industry partners' requirements.

The microcontroller from imxRT series, imxRT1060, was already used during my previous projects at the Department of Control Enegineering at the
faculty and was tested with pysimCoder on a real time control system. The open Base Board for Teensy 4.1, board designed by Czech company
PiKRON\urlnote{http://www.pikron.com/} and using imxRT1060 \glref{MCU}, provides connections to required peripherals. That resulted in selecting it as the first
target hardware. Using the already tested and supported hardware brings the advantage of not having to worry about NuttX support for peripherals as it was already
implemented during my previous projects (\glref{PWM} driver, FlexCAN driver) and focus solely on pysimCoder part. However the thesis's assignment reguired to extend
device driver support for target hardware and contribute to NuttX mainline.

The second target hardware was selected in cooperation with Czech company Elektroline a.s.\urlnote{https://www.elektroline.cz/} The company's goal is to build
its new smaller IoT devices and systems above NuttX \glref{RTOS}. Their requirements include peripherals like \glref{ADC}, Ethernet, Encoder, \glref{SPI} or
\glref{CAN}. SAM E70 \glref{MCU} from Microchip Technology was found out to be the best option, also regarding to current market situation during chip shortage,
and thus selected for the new boards designed by Elektroline. This \glref{MCU} was also used to contribute drivers extension during this thesis.

\sec imxRT1060

imxRT1060 family of microcontrollers is designed and manufactured by Dutch company NXP Semiconductors. The chips are based on ARM Cortex-M7 core running
at frequency 600 MHz and offer data and instruction cache memory of 32 KB size as well as 512 kB of data and instruction tightly coupled memory. The supported
peripherals are \glref{CAN FD}, \glref{ADC}, \glref{PWM}, Ethernet, dedicated Encoder driver, \glref{QSPI} and \glref{SPI} among others. The \glref{MCU} also
supports crossbar switches that can route inputs to outputs based on user's requirements.\cite[imxrt-basic]

\medskip \clabel[imxrt]{imxRT1060 MCU block diagram}
\picw=12cm \cinspic figs/imxrt-diagram.pdf
\caption/f imxRT1060 MCU block diagram (Source: \cite[imxrt-basic]).
\medskip

Teensy 4.1 Development Board manufactured by PJRC campeny can be used as a target hardware.\urlnote{https://www.pjrc.com/store/teensy41.html} This board
provides all peripheral pinouts, such as \glref{ADC}, \glref{PWM} or Ethernet, needed for the thesis's goals. The board however does not provide the necessary
interfaces for the drivers. This problem can be solved by Base Board for Teensy 4.1 designed by Czech company PiKRON that provides the necessary interfaces including
PMSM or DC motor control peripherals.\urlnote{https://gitlab.com/pikron/projects/imxrt-devel/-/wikis/teensy_bb} Morover, the design of the board is open and fully
fits into the thesis's theme.

\medskip \clabel[teensy-bb]{Teensy 4.1 Base Board by PiKRON}
\picw=12cm \cinspic figs/teensy-bb.jpg
\caption/f imxRT Teensy-4.1 Base Board by PiKRON.
\medskip

All necessary peripherals for the purpose of this thesis were already implemeneted by other users or during my previous projects (FlexCAN or \glref{PWM}). Base Board
for Teensy 4.1 was also tested and its capability was demonstrated with a simple DC motor position real time control.\cite[nuttx-gsoc] Having runtime monitoring
and tunning of model parameters as primary goal and leaving NuttX as secondary, this brings the advantage of having an already supported and tested peripherals
as a backup if some problems occur with another target hardware.

\sec SAM E70

Microcontrollers from SAM E70 32 bit series are designed and manufactured by American company Microchip Technology. They use ARM Cortex-M7 core running at
300 MHz and can have up to 2048  KB of Flash memory based on selected version of the microcontroller.\cite[same70-basic] The \glref{MCU}s from this series have
data and instruction cache memory of 16 KB size. The peripherals offered on SAM E70 are \glref{CAN FD}, \glref{ADC}, \glref{PWM}, Ethernet \glref{MAC}, \glref{UART},
\glref{QSPI}, \glref{SPI} and \glref{USB} host and device among others.

\medskip \clabel[sam]{SAM E70 MCU block diagram}
\picw=10cm \cinspic figs/same70-diagram.jpeg
\caption/f SAM E70 MCU block diagram (Source: \cite[same70-basic]).
\medskip

The number of supported peripherals varies by the used version of the chip. SAM E70 microcontrollers are manufactured in 64 to 144 pin
package options. The latter one offers full usage of supported peripherals while options with less pins do not offer some funtions.\cite[same70-basic]
The package version interferes with the peripheral's programming very rarely and is not taken into account in further sections.

An evaluation kit SAM E70 Xplained from Microchip was used as a target board for the peripherals tests and examples. The documentation and schematics for this
board can be found at company's website.\urlnote{https://www.microchip.com/en-us/development-tool/ATSAME70-XPLD}

NuttX did not offer support for some key peripherals at the time of the thesis assignment. The most important for real time control were \glref{ADC}, also required by
Elektroline for their projects, and \glref{PWM}. The support of \glref{QSPI} in \glref{SPI} mode was also absent and required by Elektroline but this part is not
mentioned in the following chapter since it does not have a direct connection to real time control.