\chap Runtime Monitoring and Tuning of Model Parameters

The previous chapters introduced two systems used in this thesis, NuttX and pysimCoder, and described drivers implementation to NuttX. This chapter is focused
on the main goal of the thesis, the runtime monitoring and tuning of model parameters. Both theoretical overview and implementation of Silicon Heaven
infrastructure are discussed in the following sections.

\sec Introduction

The idea behind runtime monitoring and tuning of model parameters is to allow the user to display and edit individual blocks's parameters, inputs and
outputs. Implementation of dedicated blocks for system inputs and outputs would subsequently allow the connection of distributed systems. This basically
divides the task into two subtasks: the implementation of runtime monitoring and tuning of model parameters/inputs/outputs and implementation of input and output
dedicated blocks.

As already mentioned in chapter \ref[introduction], Silicon Heaven communication infrastructure\urlnote{https://github.com/silicon-heaven} was selecteded in
order to support runtime monitoring and tuning of model parameters. The infrastructure implements ChainPack, an open remote procedure call protocol (RPC) for
data serialization that combines the properties of Extensible Markup Language (XML) and JavaScript Object Notation (JSON). The introduction of Silicon Heaven
follows up later in the chapter.

The practical part of the chapter, the implementation of the infrastructure itself, takes a lot of knowledge from the theoretical part of the thesis, especially
of chapter \ref[pysimcoder] describing pysimCoder's code generation process.

\sec Silicon Heaven Infrastructure

Silicon Heaven infrastructure (SHV) was developed at company Elektroline by Ing. František Vacek and his team. It is used in company's systems on
technology control tramway yards in Australia, Belgium and other countries around the Europe.\cite[shv-cw] The infrastructure provides core support
for many programming languages including C, C++, Python or Rust.

The infrastructure requires running a broker as a server. User applications as pysimCoder control application or GUI designed to interact with the broker are then
registered to the broker as clients. Each client can have different rights and settings based on a broker's configuration.

Apart from its main library, libshv implementing ChainPack RCP, the infrastructure provides GUI tool shvspy\urlnote{https://github.com/silicon-heaven/shvspy} and
library shvapp\urlnote{https://github.com/silicon-heaven/shvapp} with implemented applications including shvbroker. Both svhspy and shvbroker are used in the
implementation, their usage is described later.

The following section describes the protocol itself, the ChainPack \glref{RPC}.

\label[shv-rpc]
\secc ChainPack RPC

Every ChainPack \glref{RPC} message consists of three fields: length, protocol and data, respectively. The length field stores an unsigned integer of message length without
the length field itself, so the length of protocol field plus the length of data field. Protocol is once again an unsigned integer defining data format
(ChainPack \glref{RPC}, Cpon or JSON). Then data itself follows.\cite[shv-wiki-rpc]

\begtt
+--------+----------+------+
| Length | Protocol | Data |
+--------+----------+------+
\endtt

Data uses the following format. PackingSchema is uint8_t defining the type of data (integer, double, string, map, Imap, MetaMap). The examples of packed data format
can be found on Silicon Heaven wiki.\urlnote{https://github.com/silicon-heaven/libshv/wiki/ChainPack-RPC\#chainpack} The work in this thesis mostly requires the
usage of Map, IMap and MetaMap based on the unified message format. Map is basically a dictionary with string keys, IMap has integer keys and MetaMap can support both
strings and integers.

Every \glref{RPC} message data part consinsts of "<meta-data-part>" and "i{data-part}" where i represents the usage of IMap. The example of server request and client
reply taken from \glref{SHV} wiki follows.\cite[shv-wiki-login]

\begtt
<1:1,8:17,10:"hello">i{}
\endtt

Where the data in cone brackets represents "<meta-data-part>". Apart from "1:1", declaring ChainPack \glref{RPC} is used, message metadata also contains request ID
("8:17", where 8 is a tag for ID and 17 is ID itself) and requested method (10 is the tag, method's name is a string). Data part is empty in this case but the IMap
still has to be included. The client reply would be.

\begtt
<1:1,8:17>i{2:{"nonce":"1429255113"}}
\endtt

Client replies with the same ID in metadata and then sends the data itself. The number 2 indicates request result follows, in this case it is a Map of some data with a
string key. The possible tag keys can be found on Silicon Heaven wiki page.\urlnote{https://github.com/silicon-heaven/libshv/wiki/ChainPack-RPC\#rpc}

\secc ChainPack RPC Usage

Silicon Heaven infrastructure brings the support for ChainPack \glref{RPC} in many programming languages. The core functions are located in libshv library in
libshvchainpack subdirectory. The files from this directory are the only ones necessary to support the \glref{SHV} communication. These files defines functions
like "cchainpack_pack_int" or "cchainpack_pack_imap_begin" that are used to pack ChainPack \glref{RPC} message. This message can then be send or received, for
example over TCP.

\label[shv-gen]
\sec Changes to pysimCoder Code Generation Process

Several, mostly minor, changes were required in pysimCoder code generation process in order to get blocks and parameters names to C part of the code and to have a
structure that would contain all blocks used in the diagram and additional information about them. Two structures "python_block_name_entry" and
"python_block_name_map" were added to "pyblock.h" file. This file also defines the main block structure "python_block" that can access block's
parameters, inputs, outputs, dimensions and so on.

\begtt
typedef struct {
  const char * block_name;  /* Name of the block */
  int block_idx;            /* Index in python_block structure */
  int system_inputs;        /* Block has system inputs */
  int system_outputs;       /* Block has system outputs */
} python_block_name_entry;

typedef struct {
  const python_block_name_entry * blocks;   /* Pointer to
                                             * python_block_name_entry
                                             * structure */
  const python_block * block_structure;     /* Pointer to python_block
                                             * structure */
  int blocks_count;                         /* Number of blocks */
} python_block_name_map;
\endtt

The content of "python_block_name_map" is defined during code generation and the structure is then passed as an argument to \glref{SHV} tree initialization function. Integers
"system_inputs" and "system_outputs" defines whether block's inputs/outputs are used as system's inputs/outputs. This is used for the implementation of dedicated
input/output blocks.

While getting the name of the block to C code was straightforward, parameters' names were more difficult. Parameter's name is accessable in function "generateCCode"
in "scene.py" (refer to section \ref[pysim-gen] for the reminder of pysimCoder code generation process) but the type of the parameter is uknown at that moment. On the
contrary, the type is known in "genCode" function from "RCPgen.py" but the names are not available here. Therefore it was necessary to know the parameter's type
already in early code generation process in "scene.py".

I chosed to add parameters' types to xblk files so they could be accessed already in scene.py. Taking the PWM block example from section \ref[pysim-res], the updated
key params is:

\begtt
"params": "nuttx_PWMBlk|Port:'/dev/pwm2'|channels: [1]:int
                       |PWM freq [Hz]:1000:double|Umin [V]:0.0:double
                       |Umax [V]:100.0:double"
\endtt

Function "generateCCode" then process these parameters and their names and ensures they are saved to "python_block" structure so they can be accessed from C code.
The function also ensures that the generation process does not crash when the type is not defined.

The other option to get the parameter's type is to generate some sort of metadata from RCPblk function that creates the block. This function would generate
information regarding what parameters are used, which can be changed runtime and which can be just read only. This would require the RCPblk functions to be called
from the editor during block diagram creation. This aproach would be more benefitial to pysimCoder from long term view, however the implementation would be more
difficult and time consuming. Therefore I decided to go with the first option. The metadata approach can be left for some further work on the project.

\sec SHV Tree Structure

PysimCoder's blocks and their parameters, inputs and outputs are represented by items in a dedicated tree. The tree's format is following.

\begitems
\style 0
* project name
  \begitems \style 0
  * blocks
    \begitems \style 0
    * block 1
      \begitems \style 0
      * inputs
        \begitems \style 0
        * input1
        * input2
        * ...
        \enditems
      * outputs
        \begitems \style 0
        * output1
        * output2
        * ...
        \enditems
      * parameters
        \begitems \style 0
        * parameter1
        * parameter2
        * ...
        \enditems
      \enditems
    * block 2
      \begitems \style 0
      * ...
      \enditems
    * ...
    \enditems
  \enditems
\enditems

\secc Supported Methods
Every item needs to support at least two methods according to \glref{SHV} standard. Those are methods "ls" and "dir". The first method returns the list of strings
with the names of node's children. The list is empty if node does not have children. Method "dir" returns the list of methods supported by the node. In any case those
are two already mentioned methods plus item specific methods.

Every parameter's node also supports methods "set", "get" and "typeName". As the names already suggest, first two methods send and receive the parameter value.
Furthemore "typeName" returns the type of the value. Currently it is only double as integer values are not accessed by SHV but it can be enhanced in the future.

Integer values are often used for channel's numbers (for \glref{ADC} or \glref{PWM} for example) or other non-changable parameters like encoder resolution. They are
also sometimes used not as a single parameter but rather as an array of integer parameters. Correct representation of integer parameters would most likely require
metadata generation as discussed at the end of section \ref[shv-gen]. Therefore only double parameters are currently implemented in \glref{SHV} tree.

Block's inputs and outputs are read only and thus only support method "get" as well as system dedicated outputs. System inputs support both "get" and "set".

\secc Nodes' Representation
Two data structures were selected for representation of tree's items, AVL tree (called \glref{GAVL} in the library) and sorted array (called \glref{GSA}). Open
source uLAN Utilities Library (ULUT)\urlnote{https://gitlab.com/pikron/sw-base/ulut} was used for the implementation of mentioned structures to pysimCoder's code.
This library by company PiKRON provides implementation of structures and functions commonly used in C, among others AVL tree and sorted array.\cite[ulut-doc]

Usage of \glref{GAVL} is prefered when the tree is allocated dynamically during application start since \glref{GSA} uses more memory reallocation for addition of new
items to the array. However GSA can be allocated statically during code generation and all items can be constant. This allows the whole \glref{SHV} tree to be saved
to flash memory and do not waste space in RAM. The implementation of \glref{SHV} in pysimCoder supports all three options: \glref{GAVL}, \glref{GSA} and static
\glref{GSA}.

\sec SHV Tree Implementation

Previous section discussed the theoretical part of item's representation in a \glref{SHV} tree. This part introduces the structures and parts of the code taking care
of tree initialization. The tree is consisted of "shv_node_t" items. The definition of the "shv_node_t" structure can be seen below.

\begtt
typedef struct shv_node {
  const char *name;         /* Node name */
  gavl_node_t gavl_node;    /* GAVL instance */
  shv_dir_map_t *dir;       /* Pointer to supported methods */
  shv_node_list_t children; /* List of node children */
} shv_node_t;
\endtt

\glref{GAVL} instance is necessary when using AVL tree. It represents a node in a tree and links left, right and parent node to it. Structure "shv_dir_map_t" is
a \glref{GSA} array of pointers to methods supported by the node. The final field is a structure "shv_node_list_t" used as a list of children. The definition of the
mentioned structure follows.

\begtt
typedef struct shv_node_list {
  int mode;                 /* Mode selection (GAV, GSA, static) */
  union {
    struct {
      gavl_cust_root_field_t root;  /* GAVL root */
      int count;                    /* Number of root's chuldren */
    } gavl;
    struct {
      gsa_array_field_t root;       /* GSA root */
    } gsa;
  } list;
} shv_node_list_t;
\endtt

This structure keeps the mode of the node (information what type of tree is used) and \glref{GAVL} or \glref{GSA} related root structure. Note that both of those
structures are not pysimCoder related and can be used for any item in a tree. Accessing parameters/inputs/outputs in \glref{SHV} tree reqeuires one more structure.
The structure is called "shv_node_typed_val_t".

\begtt
typedef struct shv_node_typed_val {
  shv_node_t shv_node;          /* Node instance */
  void *val_ptr;                /* Value */
  char *type_name;              /* Type of the value (int, double...) */
} shv_node_typed_val_t;
\endtt

Node instance "shv_node_t" is passed to \glref{SHV} tree while "val_ptr" and "type_name" variables store a pointer to parameter's value and type name, respectively.
This additional structure is only required for parameters, inputs and outputs, other tree's items like blocks use only basic "shv_node_t".

Apart from "shv_com.c" file implementing communication funtions and described in the following section, the code is implemented in three files: "shv_tree.c",
"shv_methods.c" and "shv_pysim.c". All files are located in "CodeGen/Common/shv" directory, header files can be found in "include" subdirectory. The code structure
is designed so that general tree functions (memory allocation, search, addition of new node and so on) are located in "shv_tree.c". This file is independend on higher
level files "shv_methods.c" and "shv_pysim.c".

While general functions in "shv_tree.c" and "shv_com.c" could be used for other applications and not only for pysimCoder, the other two files implement pysimCoder
related functions. Supported methods are defined in "shv_methods.c" while "shv_pysim.c" provides an entry point and dynamic tree generation if selected.

\begtt
shv_con_ctx_t *shv_tree_init(python_block_name_map * block_map,
                             const shv_node_t *static_root, int mode)
{
  int ret;
  const shv_node_t *root;

  if ((mode & SHV_NLIST_MODE_STATIC) == 0)
    {
      /* Create tree root only if it should be allocated dynamically */

      root = shv_tree_node_new(NULL, &shv_root_dir_map, mode);
      if (root == NULL)
        {
          printf("ERROR: malloc() failed\n");
          return;
        }
      shv_tree_create(block_map, (shv_node_t *)root, mode);
    }
  else
    {
      root = static_root;
    }

  /* Initialize SHV connection */

  shv_con_ctx_t *ctx = shv_com_init((shv_node_t *)root);

  return ctx;
}
\endtt

Function "shv_tree_init", called from generated C file, is used as an entry point for \glref{SHV} related operation. This function calls "shv_tree_create()" to
create a dynamic \glref{GAVL} or \glref{GSA} tree if required and follows up with calling "shv_com_init()" from "shv_com.c". The "shv_tree_create()" function goes
throught all blocks and their parameters/inputs/outputs if supported and adds them to the tree. This process is skipped if \glref{SHV} tree is generated
statically. All tree's nodes are defined in generated C file in that case.

\sec SHV Communication

Functions securing \glref{SHV} communication are implemented in "shv_com.c "file. The entry point is a function "shv_init()" that allocates the memory for the
\glref{SHV} container taking care of data sends and receives, initializes TCP connection and performs client login to the server. The login process is standardized
and can be seen in \glref{SHV} documentation.\cite[shv-wiki-login]

Data read function is run in a separate thread so \glref{SHV} operations are executed independently from main control functions. The context of the function can
be seen below.

\begtt
static void *shv_process(void * p)
{
  int num_events;
  shv_con_ctx_t *shv_ctx = (shv_con_ctx_t *)p;

  struct pollfd pfds[1];
  pfds[0].fd = shv_ctx->stream_fd;
  pfds[0].events = POLLIN;

  while (1)
    {
      /* Set timemout to one half of shv_ctx->timeout (in ms) */

      num_events = poll(pfds, 1, (shv_ctx->timeout * 1000) / 2);

      if (num_events == 0)
        {
          /* Poll timeout, send ping */

          shv_send_ping(shv_ctx);
        }
      else if (pfds[0].revents & POLLIN)
        {
          /* Event happened on our socket, process TCP input */

          shv_process_input(shv_ctx);
        }
    }
}
\endtt

It is necessary to check for client's timeout set during login process. If server does receive any activity from client for time longer than specified timeout
than it aborts the connection. Function "poll()" takes care of determining whether it is required to ping the server. If "poll()" ends up with a
"POLLIN" event it indicates we can read data from socket.

\begtt
int shv_process_input(shv_con_ctx_t * shv_ctx)
{
  int i;
  int j;
  char met[SHV_MET_MAX_LEN];
  char path[SHV_PATH_MAX_LEN];
  struct ccpcp_unpack_context *ctx = &shv_ctx->unpack_ctx;

  i = read(shv_ctx->stream_fd, shv_ctx->shv_rd_data,
          sizeof(shv_ctx->shv_rd_data));
  if (i > 0)
    {
      ccpcp_unpack_context_init(ctx, shv_ctx->shv_rd_data, i,
                                shv_underrflow_handler, 0);

      while (ctx->current < ctx->end)
        {
          /* Get method and path */

          shv_unpack_head(shv_ctx, &j, met, path);

          if (met[0] != '\0')
            {
              shv_node_process(shv_ctx, j, met, path);
            }
        }
    }
  return i;
}
\endtt

Function "shv_process_input()" takes care of TCP read in a blocking mode. \glref{SHV} function "ccpcp_unpack_context_init()" initialize the "ccpcp_unpack_context"
structure and fills it with received data. Function "shv_underrflow_handler()" takes care of yet to be received data. This is used when received message is longer
than defined \glref{SHV} message length (1024 bytes in this case) or not all data are read by initial read because they are not delivered over TCP yet. Then
function "shv_unpack_head()" is called to unpack the metadata header part of the message and gets the information about requested method and node location in the
tree (i.e. path). Subsequently the node is found in the tree and the method is called.

The file also implements functions "shv_send_double()", "shv_send_string()" and similar that are used to send a client reply to the server request. This is used
when server asks for the block's parameter. The parameter is found in \glref{SHV} tree, as described in previous two sections, and double value is sent in reply formated
as shown in section \ref[shv-rpc]. The following code shows the definition of function "shv_send_double()". The creation of ChainPack content is done twice in
a loop in order to get the correct lenght of the message. This is something we do not know until we fill the content. Function "shv_overflow_handler" then performs
TCP write if "shv_ctx->shv_send" equals 1.

\begtt
void shv_send_double(shv_con_ctx_t *shv_ctx, int rid, double num)
{
  ccpcp_pack_context_init(&shv_ctx->pack_ctx,shv_ctx->shv_data,
                          SHV_BUF_LEN,shv_overflow_handler);

  for (shv_ctx->shv_send = 0; shv_ctx->shv_send < 2; shv_ctx->shv_send++)
    {
      if (shv_ctx->shv_send)
        {
          cchainpack_pack_uint_data(&shv_ctx->pack_ctx,
                                    shv_ctx->shv_len);
        }

      shv_ctx->shv_len = 0;
      cchainpack_pack_uint_data(&shv_ctx->pack_ctx, 1);

      shv_pack_head(shv_ctx, rid);

      cchainpack_pack_imap_begin(&shv_ctx->pack_ctx);
      cchainpack_pack_int(&shv_ctx->pack_ctx, 2);
      cchainpack_pack_double(&shv_ctx->pack_ctx, num);
      cchainpack_pack_container_end(&shv_ctx->pack_ctx);
      shv_overflow_handler(&shv_ctx->pack_ctx, 0);
    }
}
\endtt

Only TCP communication is currently supported but with further time investment \glref{SHV} could also support communication over serial port or \glref{CAN} bus. This
enhancement can be implemented in some future projects.

\sec Input/Output Blocks in SHV Tree

The changes to pysimCoder also introduce two \glref{SHV} dedicated blocks: \glref{SHV} Input and \glref{SHV} Output. These simple blocks without parameters and
with option of multiple inputs/outputs can be newly found in communication library in pysimCoder GUI. Considering SHV tree, the previously introduced tree
structure is expanded by nodes inputs and outputs as follows.

\begitems
\style 0
* project name
  \begitems \style 0
  * blocks
  * inputs
    \begitems \style 0
    * input block 1
      \begitems \style 0
      * input 1
      * input 2
      * ...
      \enditems
    \enditems
  * outputs
  \begitems \style 0
  * output block 1
    \begitems \style 0
    * output 1
    * output 2
    * ...
    \enditems
  \enditems
  \enditems
\enditems

Opposite to the parameters, individual input and output signals do not use their own names as they do not have any. They use general naming system "input0",
"input1" and so on. Inputs support both methods "get" and "set" while outputs only support "get". The support of inputs and outputs is only for those two dedicated
blocks and can not be used with other blocks from communication library. The addition of other blocks does not make much sence since those blocks
are used to generate some signal (Pulse Generator, Step and so on) or receive/send data over TCP and UDP. Dedicated \glref{SHV} blocks do not intend to replace them
but they can be used to connect multiple systems.

Source code for those blocks can be once again found in "CodeGen/Common/shv" directory under names "shv_blk_output.c" and "shv_blk_input.c". The code structure is
as described in section \ref[pysim-codegen2], the block has an entry point function and functions "init()", "inout()" and "end()" called based on a received flag. Those functions
are empty and do not perform any operations since the only purpose of the block is to have selected input or output. Their value is changed throught \glref{SHV}
method "set" and read with "get".

\sec SHV Settings in pysimCoder

This section presents compilation and configuration steps to succesfully use Silicon Heaven infrastructure with pysimCoder. \glref{SHV} support in pysimCoder is
compatible with \glref{POSIX} compliant systems GNU/Linux and NuttX. The following command is required to succesfully compile pysimCoder's source code for NuttX
target with Silicon Heaven. Apart from that the compilation steps are identical to those listed in sections \ref[nuttx-comp] and \ref[pysim-codegen2].

\begtt
make SHV=1
\endtt

The only change from standard compilation is an additional Makefile parameter "SHV=1". This parameter ensures the download of required libraries
\glref{SHV} and uLUT. It is also necessary to select support for TCP communication in NuttX configuration before the export is generated. The compilation for
GNU/Linux is similar and also requires additional parameter "SHV=1".

Silicon Heaven options like server's IP and port, user name and password, device name or type of the tree can be selected in pysimCoder's menu under \glref{SHV} support
icon.

\sec SHV Usage

Apart from ChainPack \glref{RPC}, \glref{SHV} infrastructure also offers additional applications and tools as shvbroker and shvspy mentioned earlier in this chapter.
This section shows their usage with a pysimCoder application. Package Qt 5.13\urlnote{https://doc.qt.io/archives/qt-5.13/index.html} at minimum is required to
succesfully compile shvbroker and shvapp. The latter also requires "libqt5webkit5-dev" package.\urlnote{https://packages.ubuntu.com/jammy/libqt5webkit5-dev}

Shvbroker, included in shvapp repository, acts as a \glref{TCP} server for a client (pysimCoder application, some GUI and so on). Following commands show its
compilation and usage.

\begtt
git clone https://github.com/silicon-heaven/shvapp.git
cd shvapp
git submodule update --init --recursive
qmake -r
make
cd bin
./shvbroker --config-dir ../shvbroker/etc/shv/shvbroker/ -v rpcmsg
\endtt

Command "-v rpcmsg" is optional and enables printing \glref{SHV} messages' content to terminal. Directory "shvbroker/etc/shv/shvbroker" contains configuration files
with information like server port, host name, accepted users and their passwords and so on.

Another mentioned application, shvspy, is a GUI tool for \glref{SHV} tree administration. It is connected as a client to shvbroker and allows the user to browse
throught \glref{SHV} tree and call methods. The steps to run the application are following.

\begtt
git clone https://github.com/silicon-heaven/shvspy
cd shvspy
git submodule update --init --recursive
qmake -r
make
cd bin
LD_LIBRARY_PATH=../lib/ ./shvspy -v rpcmsg
\endtt

It is also possible to download shvspy as a build binary.\urlnote{https://github.com/silicon-heaven/shvspy/actions} The last command opens a GUI application
to which user can add a new server and connect to it. Application designed with pysimCoder performs a mount to the directory selected in pysimCoder \glref{SHV}
settings. Figure 6.1 shows shvspy \glref{GUI} with an application connected to shvbroker and mounted at "test" directory.

\medskip \clabel[shvspy-app]{Silion Heaven Spy GUI Application}
\picw=14cm \cinspic figs/shvspy-app.png
\caption/f Silion Heaven Spy GUI application connected to shvbroker.
\medskip

To summarize the presented tools, an application created with pysimCoder acts as a client and communicates with a server. Server is represented by shvbroker
application. However we also want to interact with the \glref{SHV} tree and control our model. Application shvspy is used for this purpose. It is a \glref{GUI} tool
in which user can browse throught the tree.