\label[implementation]
\chap Drivers Implementation

This chapter is focused on the first practical part of the thesis, the implementation of selected periherals' drivers. All following drivers are implemented for
SAM E70 \glref{MCU} which was discussed in the previous chapter, however the implementation process would be similar and sometimes even the exactly same for other
targets. This chapter does not try to provide a step by step manual or whole process of source code creation but rather to introduce the most important steps and
parts of driver creation for NuttX. Some key code parts for the periherals' drivers are listed here but a look into the source code is recommended for the complete
understandment of the driver's functionality.

There were two major peripherals necessary for the ability of target's real time control: Analog to Digital Converted or shortly ADC and Puls Width Modulation,
PWM. The implementation of these drivers is discussed in the following sections.

\sec Analog to Digital Converter

Analog to Digital Converter (ADC) is a system that performs conversion of an analog signal (e.g. electric voltage or current) to a digital signal. This
digital signal is represented by a binary number of a finite number of bits[\rcite[meas-introduction], section 9.1, pg. ~612] (up to 16 bits in
case of SAM E70). Digital Design and Computer Architecture by Harris \& Harris can be recommended for further reading about the peripheral.\cite[ddaca-riscv]

\medskip \clabel[afec]{Analog Front End Controller Block Diagram}
\picw=12cm \cinspic figs/afec_block.pdf
\caption/f Analog Front End Controller Block Diagram (Source: [\rcite[same70-datasheet], figure 52-1].)
\medskip

Chips from SAM E70 series implement \glref{ADC} under peripheral called Analog Front End Controller (AFEC) in chip's datasheet.[\rcite[same70-datasheet], section 52]
Apart from \glref{ADC}, it integrates a programmable gain amplifier for \glref{ADC} inputs, two analog multiplexers and digital to analog converter. This allows the
\glref{MCU} to perform analog to digital conversion either of 12 lines or simuntaniously of two 6 lines. SAM E70 has 12 bit \glref{ADC} resolution by default but
this can be extended to 16 bit by digital averaging.[\rcite[same70-datasheet], section 52]

The following sections use both \glref{ADC} and \glref{AFEC} naming. \glref{AFEC} is more common when reffering to whole microcontroller's controller while 
\glref{ADC} refers to the device driver.

AFEC can be triggered either by software trigger or by external hardware trigger (e.g. PWM output, timer/counter) as can be seen in Figure 4.2. \glref{ADC}
sampling at higher frequencies (for example 10 kHz) needed for proper real time control usually requires Direct Memory Access transfers from \glref{AFEC} peripheral
to chip memory. Direct Memory Access, abbreviated as \glref{DMA}, provides a peripheral such as AFEC with a direct access to main memory. As a result, the transfer of
received data to the memory is not executed by the processor but by the \glref{DMA}. This allows the processor to execute other tasks during that time and thus
increases the system performance.\cite[dma-article]

This resulted in selecting timer/counter as \glref{ADC} trigger and implementing \glref{DMA} support for the AFEC driver. The implementation of the driver is
described in the following section.

\secc Driver Implementation

As discussed in chapter 2, source code for device drivers is located in "arch/" subdirectory. For SAM E70 MCU family, this would be "arch/arm/src/samv7" subfolder.
The lower half part of the driver is divided into three separate files in NuttX, "sam_afec.c", "sam_afec.h" and "hardware/sam_afec.h". This once again comes from the
NuttX's community consensus as mentioned in chapter 2.

The lower half part of \glref{ADC} driver communicates with the upper half located in "drivers/" via adc_dev_s structure. This structure requires the lower half
of the driver to provide two fields, ad_ops and ad_priv. The first mentioned links architecture specific operations to driver operations as setup or reset while the
latter can provide architecture specific logic as resolution or trigger selection. The following sample of the code shows definition of these fields in SAM E70
\glref{AFEC} driver.

\begtt
static const struct adc_ops_s g_adcops =
{
  .ao_bind     = afec_bind,
  .ao_reset    = afec_reset,
  .ao_setup    = afec_setup,
  .ao_shutdown = afec_shutdown,
  .ao_rxint    = afec_rxint,
  .ao_ioctl    = afec_ioctl,
};

#ifdef CONFIG_SAMV7_AFEC0
static struct samv7_dev_s g_adcpriv0 =
{
  .irq         = SAM_IRQ_AFEC0,
  .pid         = SAM_PID_AFEC0,
  .intf        = 0,
  .initialized = 0,
  .resolution  = CONFIG_SAMV7_AFEC0_RES,
#ifdef CONFIG_SAMV7_AFEC0_SWTRIG
  .trigger     = 0,
#else
  .trigger       = 1,
  .timer_channel = CONFIG_SAMV7_AFEC0_TIOACHAN,
  .frequency     = CONFIG_SAMV7_AFEC0_TIOAFREQ,
#endif
  .base          = SAM_AFEC0_BASE,
};

static struct adc_dev_s g_adcdev0 =
{
  .ad_ops      = &g_adcops,
  .ad_priv     = &g_adcpriv0,
};
\endtt

The initialization of the driver is done by the public function "sam_afec_initialize" called from board level logic which returns the corresponding adc_dev_s
structure. This structure is then registered as a device driver. Functions linked throught adc_ops_s structure are then used to setup registers, interrupts, perform
IOCTL operations and other stuffs necessary for driver functionality. These functions are called from the upper hals logic of the driver. The first method is
"afec_setup" which is called when the driver is opened for the first time. This function sets up interrupts, trigger and enables and starts \glref{DMA} if enabled.

The \glref{DMA} is implemented with so called ping-pong buffers. This means two buffers are used, \glref{DMA} collects data to the first one while the data from the
second one are processed in another thread. Then the buffers switch and worker thread reads from the first one while \glref{DMA} collects to the second one. This
ensures that data are collected by \glref{DMA} even when CPU needs to process the previously received data.

\glref{DMA} calls callback function "sam_afec_dmacallback" each times it fills the buffer with the required amount of data. This function sets up the worker thread
in which "sam_afec_dmadone" performs data read. The process of data read can be seen below.

\begtt
for (i = 0; i < priv->nsamples; i++, buffer++)
  {
    /* Get the sample and the channel number */

    chan   = (int)((*buffer & AFEC_LCDR_CHANB_MASK) >>
                  AFEC_LCDR_CHANB_SHIFT);
    sample = ((*buffer & AFEC_LCDR_LDATA_MASK) >>
              AFEC_LCDR_LDATA_SHIFT);

    if (priv->cb != NULL)
      {
        /* Give the sample data to the ADC upper half */

        priv->cb->au_receive(dev, chan, sample);
      }
  }
\endtt

The "au_receive" function saves the read data to local FIFO buffer in the upper half section of the driver. User can get the data from this buffer from application
level using the \glref{POSIX} call "read()". There are two \glref{IOCTL} calls implemented during my previous projects that helps with the buffer's operations.
ANIOC_RESET_FIFO clears the FIFO buffer and ensures all read data are new, ANIOC_SAMPLES_ON_READ returns the number of samples in the buffer.

The implementation without \glref{DMA} is mostly similar but uses corresponding interrupt that indicates there are data in Last Converted Data Register (AFEC_LCDR). The
code shown above would then read directly from this register instead of the \glref{DMA} buffer.

The \glref{ADC} can be set up by selecting following configuration options.
\begtt
CONFIG_ANALOG=y
CONFIG_ADC=y
CONFIG_SAMV7_AFEC0=y
CONFIG_SAMV7_TC0=y
CONFIG_SAMV7_TC0_TIOA0=y
\endtt
This configures basic \glref{ADC} with timer/counter trigger sampling at 1 kHz. The sampling frequency can be change by configuring "CONFIG_SAMV7_AFEC0_TIOAFREQ"
and the trigger can be change to software trigger called from application by selecting "CONFIG_SAMV7_AFEC0_SWTRIG". The channels are not selected in the
configuration but it has to be hard coded in board level section. Using \glref{DMA} requires following setup,
\begtt
CONFIG_SAMV7_XDMAC=y
CONFIG_SAMV7_AFEC_DMA=y
CONFIG_SAMV7_AFEC_DMASAMPLES=10
\endtt
which configures DMA to wait for 10 samples for each channel and then transfer it to memory. The whole source code of the driver is included in NuttX
mainline.\urlnote{https://github.com/apache/incubator-nuttx/blob/master/arch/arm/src/samv7/sam_afec.c}

\secc Application Usage

The \glref{AFEC} or \glref{ADC} peripheral can be accessed from application via standard \glref{POSIX} calls. The following code shows a simple application that opens the
driver and reads the sampled data from it. The code is simplified and for example does not check if error occurs while opening the driver or while performing read
operation. The proper application should have these checks.

\begtt
#include <nuttx/config.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>

struct adc_msg_s sample[conf_ch];
int readsize = conf_ch*sizeof(struct adc_msg_s);

int fd = open(block->str, O_RDONLY);

int nbytes = read(fd, sample, readsize);
\endtt

The conf_ch variable provides the number of configured channels. \glref{IOCTL} calls can be used to get some further information from the driver, for example
ANIOC_SAMPLES_ON_READ described in the previous section. The usage of the \glref{IOCTL} call is also simple and can be seen below.

\begtt
int ret = ioctl(fd, ANIOC_SAMPLES_ON_READ, 0);
\endtt

\sec Pulse Width Modulation

Microcontroller also needs some way to control the system. Pulse Width Modulation (PWM) peripheral is used for this purpose. This peripheral generaters a periodic output
that is pulsed high for part of the period and low for the remainder. The part of the period for which the pulse is high is called the duty cycle.[\rcite[ddaca-riscv],
chapter 9.3.7, page 542.e35] The example of PWM output can be seen in the Figure 5.2.

\medskip \clabel[pwm]{Pulse Width Modulated Signal}
\picw=12cm \cinspic figs/pwm_sig.pdf
\caption/f Sample of Pulse Width Modulate Signal (Source: [\rcite[ddaca-riscv], figure e9.17]).
\medskip

PWM can provide an analog output of designed value (voltage) and thus be used for real time syste control. PWM can be also used for other purposes
like trigger generator for \glref{ADC} and other peripherals but these options were not implemented during the thesis. Digital Design and Computer Architecture
by Harris \& Harris can be recommended for further reading regarding PWM peripheral again.\cite[ddaca-riscv]

SAM E70 \glref{MCU} implements two \glref{PWM} peripherals, each with 4 indepenent channels that can each control two complementary outputs. While the impelented
peripheral provides the option of \glref{DMA} trasnfers of duty cycle updates,[\rcite[same70-datasheet], section 21] this option was not implemented to
NuttX driver as it was not necessary for the funcionality demonstration. However this might be a place for further work and extension of NuttX
support for SAM E70.

\secc Driver Implementation

The logic behind files organization is the same as discussed in the previous section so files "sam_pwm.c", "sam_pwm.h" and "hardware/sam_pwm.h" were
created. The usage of upper and lower half structures is also largely similar to AFEC driver. The communication between lower and upper half of the
driver is done with pwm_lowerhalf_s structure. This structure requires the first field to be a pointer to the PWM callback structure pwm_ops_s, the
following fields can be architecture specific.

The following sample of the code shows declaration of required structures in "sam_pwm.c" \glref{PWM} driver.

\begtt

static const struct pwm_ops_s g_pwmops =
{
  .setup      = pwm_setup,
  .shutdown   = pwm_shutdown,
  .start      = pwm_start,
  .stop       = pwm_stop,
  .ioctl      = pwm_ioctl,
};

#ifdef CONFIG_SAMV7_PWM0

static struct sam_pwm_channel_s g_pwm0_channels[] =
{
#ifdef CONFIG_SAMV7_PWM0_CH0
  {
    .channel = 0,
    .used    = true,
    .pin     = GPIO_PWMC0_H0,
  },
#endif
#ifdef CONFIG_SAMV7_PWM0_CH1
  {
    .channel = 1,
    .used    = true,
    .pin     = GPIO_PWMC0_H1,
  },
#endif
...
};

static struct sam_pwm_s g_pwm0 =
{
  .ops = &g_pwmops,
  .channels = g_pwm0_channels,
  .channels_num = 4,
  .frequency = 0,
  .base = SAM_PWM0_BASE,
};
#endif /* CONFIG_SAMV7_PWM0 */
\endtt

From the code sample can be seen that sam_pwm_s structure is used as a pwm_lowerhalf_s and contains a pointer to \glref{PWM} operations and a several architecture
specific fields. The first initialization and registration of the device driver is done throught public function sam_pwminitialize called from board level
section. Similarly to \glref{AFEC} peripheral, this function returns pwm_lowerhalf_s and takes care of basic initialization like enabling peripheral's clock.

The first function called from the upper half of the driver, when the driver is first opened, is pwm_setup. The functions provides configuration of
peripheral registers necessary for pulse generation, however it should not enable the output and generate pulses. The pulse generation itself is
done by function pwm_start. The \glref{PWM} driver has separate 16 bits wide registers for PWM period (SAMV7_PWM_CPRD) and duty cycle (SAMV7_PWM_CDTYUPD)
for each channel. However, the option of independent frequency for each channel is currently not supported by NuttX logic and only duty cycle can differ
for each \glref{PWM} channel.

The startup function pwm_setup first need to check whether the set frequency matches with the required one and then can change the duty cycle if needed.
Update of frequency by writing to corresponding register is required if freqeuncy does not match. The process of duty cycle update can be see in the code
sample below.

\begtt
period = pwm_getreg(priv, SAMV7_PWM_CPRDX + (shift * CHANNEL_OFFSET));

/* Compute PWM width (count value to set PWM low) */

duty_pct = (duty / 65536.0) * 100;
width = (uint16_t)(((uint16_t)duty_pct * period) / 100);

/* Update duty cycle */

pwm_putreg(priv, SAMV7_PWM_CDTYUPDX + (shift * CHANNEL_OFFSET), width);

/* Enable output */

regval = CHID_SEL(1 << shift);
pwm_putreg(priv, SAMV7_PWM_ENA, regval);
\endtt

The shift variable provides the correct channel offset based on the channel number provided by upper half part of the driver. The code sample above runs
in for loop for every configured channel and updates its duty cycle by writing to the corresponding register. Duty cycle update is propagated to
control circuits after each period which may result in some channels being updated while others not. This can be solved by setting synchronous
update of all channels by writing to the corresponding register as listed below.

\begtt
/* Set sychnronous outputs */

pwm_putreg(priv, SAMV7_PWM_SCUC, SCUC_UPDULOCK);
\endtt

The PWM support can be set up by selecting following configuration option:
\begtt
CONFIG_PWM=y
CONFIG_PWM_MULTICHAN=y
CONFIG_PWM_NCHANNELS=3
\endtt
This is the common driver part configuration that sets \glref{PWM} and enables multiple channels support if required. The SAM E70 specific configuration can be
seen below.
\begtt
CONFIG_SAMV7_PWM0=y
CONFIG_SAMV7_PWM0_CH0=y
CONFIG_SAMV7_PWM0_CH1=y
CONFIG_SAMV7_PWM0_CH2=y
\endtt
It is worth mentioning the difference between \glref{PWM} and \glref{ADC} channel selection. While \glref{ADC} reqeuires the user to select channels in board
level section and thus edit NuttX source code, \glref{PWM} provides an option to set channels via configuration interface.

The source code of the described driver was accepted in NuttX mainline.\urlnote{https://github.com/apache/incubator-nuttx/blob/master/arch/arm/src/samv7/sam_pwm.c}

\secc Application Usage

The access from application level is also done via \glref{POSIX} calls similarly to \glref{AFEC} peripheral. The following code shows a simplified example and once again
should not be seen as a complete application but rather as an introduction to the most important parts of possible applications.

\begtt
#include <nuttx/config.h>
#include <nuttx/timers/pwm.h>

struct pwm_info_s info;

/* Open the device */

int fd = open(driver_path, O_RDONLY);

info.frequency = frequency;

for (int i = 0;i < n_channels; i++)
  {
    info.channels[i].channel = channel_n;
    info.channels[i].duty = channel_duty;
  }

/* Set frequency and duty cycle and start the output */

int ret = ioctl(fd, PWMIOC_SETCHARACTERISTICS,
               (unsigned long)((uintptr_t) &info));


ret = ioctl(fd, PWMIOC_START, 0);
\endtt

The \glref{IOCTL} call PWMIOC_SETCHARACTERISTICS can be also used to update the duty cycle during the code running. The \glref{PWM} logic in NuttX \glref{RTOS}
provides several helps to operate with \glref{PWM} from the application level. One of them are two defined channels numbers, 0 and -1. The first one means the channel
is not used and thus is skipped when calling PWMIOC_SETCHARACTERISTICS or PWMIOC_START and the latter indicates no following channels are used. This breaks the
for loop in which channels are set in pwm_start function and thus can save some computation time.