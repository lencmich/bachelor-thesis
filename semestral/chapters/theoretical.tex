\chap NuttX

\sec Introduction

NuttX is an open source real time operating system (RTOS) first introduced by Gregory Nutt in 2007. It has come a long way since
that and has been undergoing incubation at The Apache Software Foundation since 2019. NuttX offers a POSIX compatible environment so applications
written in compilance with POSIX standards (on Linux for example) can be run on NuttX as well without changes. Furthemore it implements ANSI
standards and some further APIs from Unix systems or some other RTOS like VxWorks.

NuttX is scalable from small 8 bits to modern 64 bits microcontrollers. This is allowed by linking from static libraries and using many source
files that contains only small number of functions. The disadvantage of this aproach is less clear source code but it allows the build system to
link only those functions that are desired by the user. The final executable can than be run for example on only 32 kB total memory (code and
data) althought typical NuttX build usually requires about at least 64 kB memory.

The desired functions like additional drivers (ADC, CAN, Ethernet), applications or systems features (tickless mode) can be selected using
Kconfig system which is taken from Linux Kernel. The configuration system offers a great freedom in choosing which features should be
included in NuttX, on the other hand it sometimes lacks proper documentation and thus sometimes it is not user friendly to select some
of those features. Some of those configurations also does not work well together and can result in build error. 

\sec Source Code Organization

The NuttX directory structure takes a lot of inspiration from Linux kernel.\cite[NuttX-dir-structure] The most importent folders from the
perspective of this thesis are subdirectories "arch/", "boards/" and "drivers/". These subdirectories contains source code and header files
for supported architectures, microcontrollers, boards and drivers. The following chapters shortly describe each of those subdirectories.

\secc Arch

The subdirectory "arch/" contains folders "include/" and "src/" for each supported microcontroller.\cite[NuttX-dir-structure] Folder
"include/" includes basic microcontroller definitions like which peripherals are supported, external interrupts or peripheral identificators.
The more importent folder from the point of this thesis is "src/" which includes all source files for hardware specific implementation for
supported drivers (also called "lower half" in NuttX documentation).\cite[NuttX-devices]

This is where NuttX varies from Linux. While Linux kernel usually implements just one controller for specific
driver, NuttX strictly implements those controllers for each microcontroller. This difference can be showned on an example with FlexCAN controller.
Linux has driver for this controller located is in "drivers/net/can/flexcan.c" while NuttX implements "flexcan.c" files in both "arch/arm/src/imxrt/"
and "arch/arm/src/s32k1xx/" MCUs subdirectories. Structures and functions from those files are connected to the "upper half" part of the driver
which provides a high-level interfaces like "read" or "write" similar way as in Linux.

The mentioned difference between NuttX and Linux comes from the {\sbf The Inviolable Principles of NuttX}, especially the part
{\sbf Sometimes Code Duplication is OK}.\cite[NuttX-principles]

Every lower half part of the driver is usually divided into three separate files in NuttX. The consensus in NuttX community is to keep the names
in format "mcu_driver.c", "mcu_driver.h" and "hardware/mcu_driver.h". The first ".c" file constains source code for the driver and takes care of
setting up the peripheral and interface with driver's upper half. The first header file usually declares just one function which can be accessed
from board level. This function takes care of initial setting of the driver and usually also returns the driver instance so it can be registered.
The latter header file located in "hardware" subfolder contains definitions of peripheral's registers and bit fields.

\secc Boards
The second subdirectory mentioned in this thesis is "boards/". This includes all necessary source files and header files for board level support
such as booting process or parts of the code that calls functions from "arch/" section that initializes drivers.

\secc Drivers
The last section of the three mentioned is "drivers/" which contains files for the "upper half" parts of the drivers as named per
NuttX documentation. The "upper half" registers itself a device name ("dev/adc0", "dev/mcan0") and implements the high level interface
such as "read", "write" or "open". The interface between the "upper half" and "lower half" is mediated via callbacks to the
"lower half" part of the driver.\cite[NuttX-devices]

\sec Supported platforms and MCUs

This section contains a non exhaustive list of platforms and MCUs supported by NuttX. Only platforms and MCUs interesting from the point of view of this
thesis and following bachelor thesis are mentioned here, the complete list can be found in NuttX documentation or in NuttX source code.

The most extended support in NuttX is for ARM instruction set architecture (ISA) which is used in most of the microcontrollers for embedded systems. This
includes versions of STM32 Nucleo, microcontrollers from i. MX RT series, LPC series or SAM series. Other architectures like Xtensa with support for
microcontrollers designed by Espressif company, widely known ISA x86 or open standard architecture RISC-V are also supported but so far not as widely as
ARM.

\chap Target Hardware Selection

The target hardware was selected in cooperation with Czech company Elektroline a.s.\urlnote{https://www.elektroline.cz/} The company's goal was to build
its new applications and systems above NuttX RTOS. The first system build with NuttX is the AN2C board designed to measure current peaks
via ADC, comunicate over CAN bus and have user input/output via LCD display and buttons. This requires the target MCU tu support ADC, SPI and CAN drivers.
Another requirement was that the MCU would already be widely supported in NuttX mainline. SAM E70 MCU version SAME70J21 from Microchip Technology
was found out to be the best option, also regarding to current market situation during chip shortage, and thus selected for the new board.

\sec SAM E70

Microcontrollers from SAM E70 32 bit series are designed and manufactured by American company Microchip Technology. They use ARM Cortex-M7 core runningat
300 MHz and can have up to 2048  KB of Flash memory based on selected version of the microcontroller.\cite[same70-basic] The MCUs from this series have
data and instruction cache memory of 16 KB size. The peripherals offered on SAM E70 are CAN FD, ADC, Ethernet MAC, UART, QSPI, SPI and USB host and device
among others.

\medskip \clabel[sam]{SAM E70 MCU block diagram}
\picw=12cm \cinspic figs/same70-diagram.jpeg
\caption/f SAM E70 MCU block diagram (Source: \cite[same70-basic].)
\medskip

The number of supported peripherals varies by the used version of the chip. SAM E70 microcontrollers are manufactured in 64 to 144 pin
package options. The latter one offers full usage of supported peripherals while options with less pins does not offer some funtions.\cite[same70-basic]

The following sections contains basic information regarding peripherals that were implemented into NuttX or introduced with major changes during
my work on this project. The AN2C board, is described later in the thesis.

\sec Analog to Digital Converter

Analog to Digital Converter (ADC) is a system that performs conversion of an analog signal (e.g. electric voltage or current) to a digital signal. This
digital signal is represented by a binary number of a finite number of bits[\rcite[meas-introduction], section 9.1, pg. ~612] (up to 16 bits in
case of SAM E70).

Chips from SAM E70 series implement ADC under peripheral calles Analog Front End Controller (AFEC) in chip datasheet.[\rcite[same70-datasheet], section 52]
Apart from ADC, it integrates a programmable gain amplifier for ADC inputs, two analog multiplexers and digital to analog converter. This allows the
MCU to perform analog to digital conversion either of 12 lines or simuntaniously of two 6 lines. SAM E70 has 12 bit ADC resolution by default but
this can be extendet to 16 bit by digital averaging.[\rcite[same70-datasheet], section 52]

\medskip \clabel[afec]{Analog Front End Controller Block Diagram}
\picw=12cm \cinspic figs/afec_block.png
\caption/f Analog Front End Controller Block Diagram (Source: [\rcite[same70-datasheet], figure 52-1].)
\medskip

\secc Implementation in NuttX

ADC can be triggered either by software trigger or by external hardware trigger (e.g. PWM output, timer/counter). Elektroline needs the ADC to sample
at frequency 10 kHz which requires DMA transfers from AFEC peripheral to chip memory. This resulted in selecting timer/counter as ADC trigger and
implementing DMA support to the AFEC driver.

The lower half of the driver is located in "Arch" section as discussed in chapter
2.1.1\urlnote{https://github.com/apache/incubator-nuttx/blob/master/arch/arm/src/samv7}. The ADC can be set up by selecting following
configuration option:
\begtt
CONFIG_ANALOG=y
CONFIG_ADC=y
CONFIG_SAMV7_AFEC0=y
CONFIG_SAMV7_TC0=y
CONFIG_SAMV7_TC0_TIOA0=y
\endtt
This configures basic ADC with timer/counter trigger sampling at 1 kHz. The sampling frequency can be change by configuring "CONFIG_SAMV7_AFEC0_TIOAFREQ"
or the trigger can be change to software trigger called from application by selecting "CONFIG_SAMV7_AFEC0_SWTRIG". Using DMA requires following setup:
\begtt
CONFIG_SAMV7_XDMAC=y
CONFIG_SAMV7_AFEC_DMA=y
CONFIG_SAMV7_AFEC_DMASAMPLES=10
\endtt
which configures DMA to wait for 10 samples for each channel and then transfer it to memory.

\sec Quad Serial Peripheral Interface

The AN2C board uses LCD display connected via Serial Peripheral Interface (SPI) Bus. The support for this peripheral is provided only in some version
of the SAM E70 MCUs. The other versions, SAME70J21 included, needs to use Quad Serial Peripheral Interface (QSPI) in SPI mode. Configuration of QSPI
in SPI Mode is almost the same as the configuration of SPI peripherals. Just two data input/ouput pins (MOSI and MISO) are used in single bit SPI mode
instead of four data input/outpu pins in QSPI mode.[\rcite[same70-datasheet], section 42]

\medskip \clabel[qspi]{Quad Serial Peripheral Interface Block Diagram}
\picw=12cm \cinspic figs/qspi_block.png
\caption/f Quad Serial Peripheral Interface Block Diagram (Source: [\rcite[same70-datasheet], figure 42-1].)
\medskip

\secc Implementation in NuttX

One option was to implement functions for SPI mode directly to lower half of QSPI peripheral driver (which was already supported in NuttX in QSPI mode)
but discussion with NuttX community resulted in implementation in separate files called "sam_qspi_spi.*".

\begtt
CONFIG_SPI=y
CONFIG_SPI_CMDDATA=y
CONFIG_SAMV7_QSPI=y
CONFIG_SAMV7_QSPI_SPI_MODE=y
\endtt

\sec Controller Area Network

Controller Area Network (CAN) is a networ protocol defined in the ISO 11898 standards. It was originally designed for reliable control networks in
automotive industry but later it found its usage in also other fields. CAN standard has been later extended with flexible data rate CAN FD standard
which can tramsit and receive data with higher frequency. CAN standards use non destructive CSMA collision resolution (CSMA/CR)
which ensures that there is neither data nor time is lost when collision occurs.[\rcite[CAN-article]]

\secc Implementation in NuttX

The SAM E70 chips uses MCAN module that performs communication according to ISO 11898-1:2015 standard or to Bosch CAN-FD specification based on
selected configuration.[\rcite[same70-datasheet], section 49] The support for SAM E70's lower half driver of MCAN was already integrated in NuttX
source but it was supporting only revision A of the chip. This revision is no longer in production as Microchip came with revision B that fixes
some problems in MCAN peripheral. The transition to revision B was already done by Miloš Pokorný from Elektroline so the only necessary work was to
impelement those changes to NuttX mainline so the driver would support both revisions. The differences from implementation point of view were mostly
between offsets of some register's bit fields.

The code uses Chip Identifier registers to determine itself what is the chip's revision and selects corresponding registers and bit fields. The MCAN
can be configured by:

\begtt
CONFIG_CAN=y
CONFIG_SAMV7_MCN0=y
CONFIG_SAMV7_MCAN_QUEUE_MODE=y
\endtt

The same configuration option can be done if MCAN1 is used instead of MCAN0. Optional CAN FD can be turn on by
"CONFIG_CAN_CANFD". Arbitration and data baud rate can be modified by "CONFIG_SAMV7_MCAN0_BITRATE" and "CONFIG_SAMV7_MCAN0_FBITRATE" respectively.

\chap AN2C board

AN2C board is a board designed and developed by a Czech company Elektroline. This board uses SAME70J21 MCU in 64 pin version and its main function is
to measure current peaks via ADC. Communication with the board is provided via CAN bus which can be used for example for uploading a new version of
software. Buttons can be used for user input while attached LCD display can provide a simple GUI. The block diagram of used peripherals can be seen
in Figure 4.1.

\medskip \clabel[an2c-d]{Block diagram of AN2C board peripherals.}
\picw=12cm \cinspic figs/an2c-diagram.png
\caption/f Block diagram of AN2C board peripherals.
\medskip

The application for this board is build under NuttX environment. This brings the advantage of writing an application in POSIX environment and also
having the application portable to another hardware supported by NuttX.

\medskip \clabel[an2c-p]{AN2C board without upper case.}
\picw=12cm \cinspic figs/an2c-photo.jpg
\caption/f AN2C board without upper case.
\medskip

\sec Board Support Package

Board support package (BSP) for AN2C board is based on BSP for SAM E70 Xplained board. The latter mentioned is an evaluation kit from Microchip
company\urlnote{https://www.microchip.com/en-us/development-tool/ATSAME70-XPLD} already supported by NuttX. The evaluation kit uses similar MCU
(SAME70Q21, difference between chips is only in number of output pins).

Full code of AN2C BSP can be found in my GitHub repository,\urlnote{https://github.com/michallenc/incubator-nuttx/tree/an2c/boards/arm/samv7/p-an2c}
following paragraphs contains only the most important parts of the port for the board.

Entry point for boards with SAM E70 MCUs is function "sam_boardinitialize()" located in "sam_boot.c" file. This function is called from
architecture logic after configuration of chip clocks and memory but before any peripheral is initialized. It takes care of initializing
low level things including GPIO pins for LEDs, USB or SPI. It is not possible to set up device drivers at this point because the operating
system has not been initialized yet.\cite[NuttX-board]

Another entry point is the function "board_late_initialize()" which is called from the common NuttX logic. Operating system is initialized at
this point so board specific drivers like ADC, CAN, LCD and so on can be initialize here. The definition of function "board_late_initialize()"
may look like

\begtt
#ifdef CONFIG_BOARD_LATE_INITIALIZE
void board_late_initialize(void)
{
  /* Perform board initialization */

  sam_bringup();
}
#endif /* CONFIG_BOARD_LATE_INITIALIZE */
\endtt

Function "sam_bringup()" is located in "sam_bringup.c" and takes care of deciding which driver initialization should be called based on selected
configuration options. The following part of code shows the example for initialization of MCAN and Encoder.

\begtt
#ifdef CONFIG_SAMV7_MCAN
  /* Initialize CAN and register the CAN driver. */

  ret = sam_can_setup();
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: sam_can_setup failed: %d\n", ret);
    }
#endif

#ifdef CONFIG_SENSORS_QENCODER

  ret = sam_gpio_enc_init();
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: sam_gpio_enc_init failed: %d\n", ret);
    }
#endif
\endtt

Functions "sam_can_setup()" and "sam_gpio_enc_init()" are defined in separate files named based on driver's name ("sam_mcan.c", "sam_gpio_enc.c").

The OS logic calls "board_late_initialize()" only if configuration option "CONFIG_BOARD_LATE_INITIALIZE" is turned on. The other option is to
initialize the device drivers from "board_app_initialize()" function located in "sam_appinit.c". This function is called from application side
via the "boardctl()" interface using the command "BOARDIOC_INIT". This call is automaticaly done with the initialization of a NuttShell (NSH)
application which is included in NuttX build by default.\cite[NuttX-NSH] The initialization of device drivers is done throught
"sam_bringup()" as described above.

The described driver initialization proccess is consider a standard across NuttX community and is common for most of the boards supported in
NuttX mainline.

\sec Implementation of Encoder input

The AN2C board uses incremental encoder for user input (can be used for navigation in application menu for example). The SAM E70 MCUs supports
Encoder driver via Timers but this option can be used since outputs of the encoder are not connected to the correct Timer's pins. The usage
of a proper driver might also be unnecessary for simple incremental encoder.

The alternative was to use pin's inputs to generate interrupts on a rising edge and then proccess these interrupts and determine whether the
position of the encoder should be incremented or decremented. The goal was to create this lower half of a driver in board level section and
connect it to classic encoder upper half so it could be use the same way as a proper encoder driver. While this type of driver implementation
is not correct by NuttX standards, it was the best option for Elektroline's usage.

The deccision process of encoder position incrementation when rising edge interrupt is received is showned below. The code is taken from
file "sam_gpio_enc.c" in AN2C BSP.

\begtt

/* Read the status of encoder pins */

stateA = sam_gpioread(gpio_enc_pins[0]);
stateB = sam_gpioread(gpio_enc_pins[1]);

new = (stateB << 1 | (stateA^stateB));
incr = ((new - priv->position + 1) & 3) - 1;
incr_mask = (int32_t)((1 - incr) >> 2);

/* Increment position */
priv->position += incr & ~incr_mask;

/* Count error */

priv->error -= incr_mask;
\endtt

The "incr_mask" is used for error detection. If increment would be greater than 1 the mask would clear the addition and saved the error to
private structure. The error is currently not accessable from application side but IOCTL call could be add to access it. The current position
of the encoder "priv->position" is passed to application layer throught upper half of the driver.

The implemention of lower half of the driver in board layer is not correct according to NuttX standards as was mentioned above but the code
from "sam_gpio_enc.c" could be generalized and merged into NuttX mainline as a lower half interface for boards based on chips without encoder
driver. The coding style would be similar to general lower half part for
GPIO.\urlnote{https://github.com/apache/incubator-nuttx/blob/master/drivers/ioexpander/gpio_lower_half.c}

\sec NuttX compilation

The following steps are required to compile NuttX for AN2C board. The compilation steps are done for Linux distribution Ubuntu (version 20.04 and
newer) but this should be the same for others Linux distributions. Compilation of NuttX on Windows or Mac OS is also possible and is described
in NuttX documentation.

\begtt
git clone https://github.com/michallenc/incubator-nuttx.git nuttx
git clone https://github.com/michallenc/incubator-nuttx-apps.git apps
cd apps
git checkout an2c
cd ..
cd nuttx
git checkout an2c
./tools/configure.sh p-an2c:max
make
\endtt

These steps compile NuttX executible "nuttx.bin" that can be flashed into board for example via OpenOCD connection. It might be necessary to run a
command "mww 0x400e0c04 0x5a00010b" in OpenOCD for the first time the program is flashed into board. This command ensures that the code is loaded
from flash memory instead of SRAM.